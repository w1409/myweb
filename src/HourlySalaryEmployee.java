class HourlySalaryEmployee extends StaffMember{
    private int hourWorked;
    private double rate;
    private  double payment;
    public HourlySalaryEmployee(int id, String name, String address, int hourWorked, double rate) {
        super(id, name, address);
        this.hourWorked = hourWorked;
        this.rate = rate;
        this.payment=payment;
    }
    public double pay(){
        return hourWorked*rate;
    }
    public int getHourWorked() {
        return hourWorked;
    }

    public double getRate() {
        return rate;
    }

    @Override
    public String toString() {
        return "HourlySalaryEmployee" +"\n"+
                "id= " + id +"\n"+
                "name= " + name +"\n"+
                "address= " + address +"\n"+
                "hourwork= " + hourWorked +"\n"+
                "rate= " + rate +"\n"+
                "payment= "+pay()
                ;
    }

    public double getPayment(){
        return payment;
    }

    HourlySalaryEmployee(int id, String name, String address) {
        super(id, name, address);
    }
}