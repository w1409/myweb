class SalariedEmployee extends StaffMember{
    private double salary;
    private double bonus;
    private double payment;

    public SalariedEmployee(int id, String name, String address, double salary, double bonus){
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
        }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "SalariedEmployee"+"\n"+
                "id= " + id+"\n"+
                "name= " + name+"\n" +
                "address = " + address+"\n" +
                "salary =" + salary+"\n"+
                "bonus= "+ bonus+"\n"+
                "payment= "+pay();
    }

    public double getBonus() {
        return bonus;
    }

    public double getPayment() {
        return payment;
    }

    SalariedEmployee(int id, String name, String address) {
        super(id, name, address);
    }

    public double pay(){
        return salary+bonus;
    }
}