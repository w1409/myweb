class Volunteer extends StaffMember{
    private  double salary;
    Volunteer(int id, String name, String address, double salary) {
        super(id, name, address);
        this.id=id;
        this.name=name;
        this.address=address;
        this.salary=salary;
    }

    public void setSalary() {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Volunteer" +"\n"+
                "id= " + id +"\n"+
                "name= " + name +"\n"+
                "address =" + address +"\n"+
                "salary= " + salary
                ;
    }

    public void setId(){
        this.id=id;
    }

    public void setName(){
        this.name=name;
    }

    public void setAddress(){
        this.address=address;
    }
    public double pay(){

        return pay();
    }

}